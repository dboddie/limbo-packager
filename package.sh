#!/dis/sh

load std
load expr

subfn splitpath {
    pieces = ${split '/' $*}
    dirpieces = ()
    l = $#pieces
    i = 1
    for piece in $pieces {
        if {! ~ $i $l} {
            dirpieces = $dirpieces $piece
        }
        i = ${expr $i 1 +}
    }
    dirpath = ${join '/' $dirpieces}
    # Add a leading separator if required.
    if {~ $* '/*'} {
        dirpath = '/'^$dirpath
    }
    result = $dirpath $piece
}

subfn isdir {
    arg = ${index 1 $*}
    res = `{ls -l -d $arg >[2] /dev/null}  # ` # for syntax highlighting

    if {~ $#res 0} {
        result = -1     # not found
    } {~ $res 'd*'} {
        result = 1      # directory
    } {
        result = 0      # file
    }
}

subfn install {
    (fname installpath file_or_dir) = $*
    (dir name) = ${splitpath $fname}
    destdir = $installpath^'/'^$dir
    destpath = $destdir^'/'^$name

    if {~ $opt_dryrun 1} {
        echo $fname ' -> ' $destpath
    } {
        mkdir -p $destdir
        d = ${isdir $fname}
        if {~ $d 1} {
            cp -r $fname $destpath
        } {~ $d 0} {
            cp $fname $destpath
        } {! ~ $file_or_dir '*\**'} {
            # Create files or directories if their paths are not wildcarded.
            if {~ $file_or_dir 'file'} {
                touch $destpath
            } {
                mkdir $destpath
            }
        }
    }
}

subfn install_file {
    ${install $* 'file'}
}

subfn install_dir {
    ${install $* 'dir'}
}

# Reads the file supplied as an argument.
# Creates variables for the main file, other files and directories, options and
# script.
subfn readpkg {
    pkgpath = $*
    in_section = ''
    main = files = dirs = options = script = ()

    getlines {
        if {~ $line 'main:'} {
            in_section = 'main'
        } {~ $line 'files:'} {
            in_section = 'files'
        } {~ $line 'dirs:'} {
            in_section = 'dirs'
        } {~ $line 'options:'} {
            in_section = 'options'
        } {~ $line 'script:'} {
            in_section = 'script'
        } {! ~ $line ''} {
            if {~ $in_section 'main'} {
                main = $line
            } {~ $in_section 'files'} {
                # Expand any wildcards before appending paths.
                expanded = `{'{ ls '^$line^' } >[2] /dev/null'}    # `
                if {! ~ $#expanded 0} {
                    files = $files $expanded
                } {
                    files = $files $line
                }
            } {~ $in_section 'dirs'} {
                # Expand any wildcards before appending paths.
                expanded = `{'{ ls '^$line^' } >[2] /dev/null'}    # `
                if {! ~ $#expanded 0} {
                    dirs = $dirs $expanded
                } {
                    dirs = $dirs $line
                }
            } {~ $in_section 'options'} {
                if {~ $line 'wm'} {
                    files = '/dis/wm/wm.dis' $files
                    opt_wm = 1
                } {~ $line 'tk'} {
                    files = '/dis/lib/tkclient.dis' $files
                    opt_tk = 1
                }
                options = $options $line
            } {~ $in_section 'script'} {
                script = ($script
                $line)
            }
        }
    } < $pkgpath
}

# Main

args = $*

opt_wm = opt_tk = opt_dryrun = opt_help = opt_force = 0
fileargs = ()

for arg in $* {
    if {~ $arg '-n'} {
        opt_dryrun = 1
    } {~ $arg '-h'} {
        opt_help = 1
    } {~ $arg '-f'} {
        opt_force = 1
    } {
        fileargs = $fileargs $arg
    }
}

if {or {~ ${expr $#fileargs 2 lt} 1} {~ opt_help 1}} {
    echo 'usage: package.sh [-h] [-f] [-n] pkgfile directory'
    exit 1
}

pkgfile = ${index 1 $fileargs}
outdir = ${index 2 $fileargs}

if {and {~ ${isdir $outdir} 1} {~ $opt_force 0} {~ $opt_dryrun 0}} {
    echo $outdir 'already exists'
    exit 1
}

# Read the package file, updating the main, files, dirs, options and script
# variables.
${readpkg $pkgfile}

files = '/dis/emuinit.dis' $main $files 

allfiles = ()

# Compile a list of all the specified files, including dis file dependencies.
for fname in $files {
    allfiles = $allfiles $fname
    if {~ $fname '*.dis'} {
        for deppath in `{disdep $fname} {   # `
            allfiles = $allfiles $deppath
        }
    }
}

# The last name should be the first path specified on the command line.
appname = $fname

# Copy all the files and directories found so far to the output directory.
for fname in $allfiles {
    ${install_file $fname $outdir}
}

for fname in $dirs {
    ${install_dir $fname $outdir}
}

# Also install icons and fonts, if requested.
if {or {~ $opt_tk 1} {~ $opt_wm 1}} {
    for fname in `{ls /icons/tk} {      # `
        if {~ ${isdir $fname} 0} {
            ${install $fname $outdir}
        }
    }
    for fname in `{ls /fonts/pelm} {    # `
        if {~ ${isdir $fname} 0} {
            ${install $fname $outdir}
        }
    }
}

if {~ $opt_dryrun 0} {
    # Write the invocation script to the output directory.
    scriptfile = $outdir^'/run'
    echo '#!/dis/sh' > $scriptfile
    for line in $script {
        echo $line >> $scriptfile
    }

    chmod 0755 $scriptfile
}
